import java.util.Scanner;

public class Student {
    public String name;
    public String surname;
    public int age;
    public String group;

    public Student() {


    }


    @Override
    public String toString() {
        return String.format("Имя: %s Фамилия: %s Возраст: %d Группа: %s \n",name,surname,age,group);


    }
     public Student(String name, String surname, int age, String group) {
         this.name = name;
         this.surname  = surname;
         this.age = age;
         this.group = group;
    }
}
